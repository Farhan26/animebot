package com.jojonicho.animebot.discorduser.service;

import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.repository.DiscordUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DiscordUserServiceImplTest {

    @Mock
    private DiscordUserRepository discordUserRepository;

    @InjectMocks
    private DiscordUserServiceImpl discordUserService;;

    private DiscordUser discordUser;

    private AnimeEntry mockAnimeEntry;

    @BeforeEach
    public void setUp(){
        discordUser = new DiscordUser();
        discordUser.setId("1");

        mockAnimeEntry = new AnimeEntry();
        mockAnimeEntry.setDiscordUser(discordUser);
        mockAnimeEntry.setMalId(1);
    }

    @Test
    public void testServiceCreate() {
        when(discordUserRepository.save(any()))
                .thenReturn(discordUser);
        DiscordUser actual = discordUserService.createDiscordUser(discordUser.getId());
        assertEquals(discordUser, actual);
    }

    @Test
    public void testServiceGetDiscordUserNoUser() {
        when(discordUserRepository.getDiscordUserById(discordUser.getId()))
               .thenReturn(null);
        when(discordUserRepository.save(any()))
                .thenReturn(discordUser);

        DiscordUser actual = discordUserService.getDiscordUser(discordUser.getId());
        assertEquals(discordUser, actual);
    }

}
