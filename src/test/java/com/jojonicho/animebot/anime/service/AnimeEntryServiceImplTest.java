package com.jojonicho.animebot.anime.service;

import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.anime.repository.AnimeEntryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AnimeEntryServiceImplTest {

    @Mock
    private AnimeEntryRepository animeEntryRepository;

    @InjectMocks
    private AnimeEntryServiceImpl animeEntryService;

    private DiscordUser discordUser;

    private AnimeEntry mockAnimeEntry;

    @BeforeEach
    public void setUp(){
        discordUser = new DiscordUser();
        discordUser.setId("1");

        mockAnimeEntry = new AnimeEntry();
        mockAnimeEntry.setDiscordUser(discordUser);
        mockAnimeEntry.setMalId(1);
    }

    @Test
    public void testServiceCreate() {
        when(animeEntryRepository.save(any()))
                .thenReturn(mockAnimeEntry);
        AnimeEntry animeEntry = animeEntryService.createAnimeEntry(1, discordUser);
        assertEquals(animeEntry, mockAnimeEntry);
    }

    @Test
    public void testServiceCreateDuplicateAnime() {
        when(animeEntryRepository.findByMalIdAndDiscordUser(mockAnimeEntry.getMalId(), discordUser))
                .thenReturn(mockAnimeEntry);

        AnimeEntry animeEntry = animeEntryService.createAnimeEntry(1, discordUser);
        assertEquals(animeEntry, mockAnimeEntry);
    }

    @Test
    public void testServiceFindByUserId() {
        List<AnimeEntry> list = new ArrayList<>();
        list.add(mockAnimeEntry);

        when(animeEntryRepository.findAllByDiscordUser_Id(discordUser.getId()))
                .thenReturn(list);

        Iterable<AnimeEntry> actual = animeEntryService.getListAnimeEntry(discordUser.getId());
        assertEquals(list, actual);
    }

}
