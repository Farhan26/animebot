package com.jojonicho.animebot;

import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.jagrosh.jdautilities.examples.command.AboutCommand;
import com.jagrosh.jdautilities.examples.command.PingCommand;
import com.jagrosh.jdautilities.examples.command.ShutdownCommand;
import com.jojonicho.animebot.anime.commands.*;
import com.jojonicho.animebot.example.commands.CatCommand;
import com.jojonicho.animebot.example.commands.ChooseCommand;
import com.jojonicho.animebot.example.commands.HelloCommand;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import javax.security.auth.login.LoginException;
import java.awt.*;

@SpringBootApplication
public class AnimeBotApplication {

    private static String TOKEN;
    private static String OWNER_ID;
    private static String PREFIX = "$";

    @Autowired
    AnimeEntryService animeEntryService;
    @Autowired
    DiscordUserService discordUserService;
    EventWaiter waiter;
    CommandClientBuilder client;

    @Autowired
    public AnimeBotApplication(@Value("${discord_token}") String token,
                               @Value("${discord_owner_id}") String ownerId) {
        TOKEN = token;
        OWNER_ID = ownerId;
        waiter = new EventWaiter();
        client = new CommandClientBuilder();
    }

    public static void main(String[] args) throws IllegalArgumentException {
        SpringApplication app = new SpringApplication(AnimeBotApplication.class);
        app.run();
    }

    private void initClient() {
        // The default is "Type -help" (or whatver prefix you set)
        client.useDefaultGame();
        // sets the owner of the bot
        client.setOwnerId(OWNER_ID);
        // sets emojis used throughout the bot on successes, warnings, and failures
        client.setEmojis("\uD83D\uDE03", "\uD83D\uDE2E", "\uD83D\uDE26");
        // sets the bot prefix
        client.setPrefix(PREFIX);
    }

    @PostConstruct
    public void run() throws LoginException, IllegalArgumentException, RateLimitedException {
        initClient();

        client.addCommands(
                // command to show information about the bot
                new AboutCommand(Color.BLUE, "an example bot",
                        new String[]{"Cool commands","Nice examples","Lots of fun!"},
                        Permission.ADMINISTRATOR),

                // command to shut off the bot
                new ShutdownCommand()
        );

        addAnimeCommands();
        // addMangaCommands();
        // addMiniGamesCommands();
        // addFriendCommands();
        // addScheduleNotifierCommands();
        addExampleCommands();

        // start getting a bot account set up
        JDABuilder.createDefault(TOKEN)
                // set the game for when the bot is loading
                .setStatus(OnlineStatus.DO_NOT_DISTURB)
                .setActivity(Activity.playing("watching some anime..."))
                // add the listeners
                .addEventListeners(waiter, client.build())
                // start it up!
                .build();
    }

    void addAnimeCommands() {
        // anime commands
        client.addCommands(
                new NextSeasonSearchCommand(),
                new SeasonSearchCommand(),
                new AnimeListCommand(animeEntryService, discordUserService),
                new AnimeCommand(animeEntryService, discordUserService),
                new AnimeSearchCommand()
        );
    }

    void addExampleCommands() {
        client.addCommands(
                // examples
                new CatCommand(),
                new ChooseCommand(),
                new HelloCommand(waiter),
                new PingCommand()
        );
    }
}
