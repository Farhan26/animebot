package com.jojonicho.animebot.anime.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.experimental.Accessors;

import java.sql.Timestamp;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class JikanAnime {
    private int malId;
    private String url;
    private String imageUrl;
    private String title;
    private boolean airing;
    private String synopsis;
    private String type;
    private int episodes;
    private double score;
    private Timestamp startDate;
    private Timestamp endDate;
    private int members;
    private String rated;
}
