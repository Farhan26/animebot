package com.jojonicho.animebot.anime.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

////@Embeddable
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//class AnimeEntryPK implements Serializable {
//    private String malId;
//    private DiscordUser discordUser;
//}


@Entity
@Table(name = "anime_entry")
@Data
@NoArgsConstructor
@AllArgsConstructor
//@IdClass(AnimeEntryPK.class)
public class AnimeEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", insertable = false, updatable = false, nullable = false)
    Long id;

    @Column(name = "mal_id", updatable = false, nullable = false)
    private int malId;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "discord_user_id", updatable = false, nullable = false)
    private DiscordUser discordUser;

    @Column(name = "status")
    private String status;

    @Column(name = "rating")
    private int rating;

//    @EmbeddedId
//    private AnimeEntryKey key;

//    @JsonIgnore
////    @JsonBackReference
//    @OneToMany(mappedBy = "asisten", fetch = FetchType.EAGER)
//    private List<Log> logs;

//    //    @JsonIgnore
//    @Column(name ="password")
//    private String password = "admin"; // default
}
