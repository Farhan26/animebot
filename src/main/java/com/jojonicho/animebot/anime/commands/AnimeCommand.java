package com.jojonicho.animebot.anime.commands;

import com.github.doomsdayrs.jikan4java.core.Connector;
import com.github.doomsdayrs.jikan4java.data.model.main.anime.Anime;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import lombok.SneakyThrows;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.ChannelType;

import java.awt.Color;

public class AnimeCommand extends Command {

    private AnimeEntryService animeEntryService;
    private DiscordUserService discordUserService;

    public AnimeCommand(AnimeEntryService animeEntryService, DiscordUserService discordUserService) {
        this.name = "anime";
        this.aliases = new String[]{"a"};
        this.help = "add anime/manga to list";
        this.arguments = "<add/update/delete> <anime/manga id> <update:status> <update:rating>";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
        this.animeEntryService = animeEntryService;
        this.discordUserService = discordUserService;
    }

    @SneakyThrows
    @Override
    protected void execute(CommandEvent event) {
        if (event.getArgs().isEmpty()) {
            insufficientArgumentErrorReply(event);
            return;
        }

        String[] args  = event.getArgs().split("\\s+");

        String command = args[0];
        int id = Integer.parseInt(args[1]);

        String newStatus = null;
        int rating = 0;

        if (args.length >= 4) {
            newStatus = args[2];
            rating = Integer.parseInt(args[3]);
        }

        if (command.equalsIgnoreCase("update") && (null == newStatus || rating == 0)) {
            insufficientArgumentErrorReply(event);
            return;
        }

        String discordUserId = event.getAuthor().getId();
        DiscordUser discordUser = discordUserService.getDiscordUser(discordUserId);
        Anime anime = new Connector().retrieveAnime(id).get();
        int malId = anime.getMalID();
        String title = String.format("%d - %s", anime.getMalID(), anime.getTitle());

        if (command.equalsIgnoreCase("add")) {
            AnimeEntry animeEntry = animeEntryService.createAnimeEntry(malId, discordUser);

            String description = String.format("Successfully added %s to your list!", anime.getTitle());

            event.reply(new EmbedBuilder()
                    .setColor(event.isFromType(ChannelType.TEXT) ? event.getSelfMember().getColor() : Color.GREEN)
                    .setTitle(title)
                    .setDescription(description)
                    .setImage(anime.getImageURL())
                    .build());

        } else if (command.equalsIgnoreCase("update")) {
            AnimeEntry animeEntry = animeEntryService.getAnimeEntry(malId, discordUser);
            if (null == animeEntry) {
                animeEntryNotFoundErrorReply(event, anime);
                return;
            }
            animeEntry.setRating(rating);
            animeEntry.setStatus(newStatus);
            animeEntryService.updateAnimeEntry(animeEntry);
            event.reactSuccess();

        } else if (command.equalsIgnoreCase("delete")) {
            AnimeEntry animeEntry = animeEntryService.getAnimeEntry(malId, discordUser);
            if (null == animeEntry) {
                animeEntryNotFoundErrorReply(event, anime);
                return;
            }
            animeEntryService.deleteAnimeEntry(animeEntry);
            event.reactSuccess();
        }
    }

    private void animeEntryNotFoundErrorReply(CommandEvent event, Anime anime) {
        String title = String.format("You don't have %d - %s in your list!",
                        anime.getMalID(), anime.getTitle());
        String description = String.format("type $anime add %d before update / delete",
                        anime.getMalID());
        event.reply(new EmbedBuilder()
                .setColor(event.isFromType(ChannelType.TEXT) ? event.getSelfMember().getColor() : Color.GREEN)
                .setTitle(title)
                .setDescription(description)
                .build());
        event.reactError();
    }

    private void insufficientArgumentErrorReply(CommandEvent event) {
        event.reply(new EmbedBuilder()
                .setColor(event.isFromType(ChannelType.TEXT) ? event.getSelfMember().getColor() : Color.GREEN)
                .setTitle("Insufficient arguments!")
                .setDescription("type $help for the list of commands and their arguments")
                .build());
        event.reactError();
    }
}
