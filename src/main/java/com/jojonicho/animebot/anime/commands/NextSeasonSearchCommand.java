package com.jojonicho.animebot.anime.commands;

import com.github.doomsdayrs.jikan4java.core.Connector;
import com.github.doomsdayrs.jikan4java.data.model.main.season.SeasonSearch;
import com.github.doomsdayrs.jikan4java.data.model.main.season.SeasonSearchAnime;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

import lombok.SneakyThrows;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.ChannelType;

import java.awt.*;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class NextSeasonSearchCommand extends Command {
    final private static int NORMAL_LIMIT = 5;
    final private static int HARD_LIMIT = 20;

    public NextSeasonSearchCommand() {
        this.name = "season-search-next";
        this.aliases = new String[]{"ssn"};
        this.help = "shows anime for next season";
        this.arguments = "<number of animes>";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
    }

    @SneakyThrows
    @Override
    protected void execute(CommandEvent event) {

        boolean hasArgs = !event.getArgs().isEmpty();
        String[] args  = hasArgs ? event.getArgs().split("\\s+") : new String[]{};
        int numAnimes = NORMAL_LIMIT;

        if (hasArgs) {
            numAnimes = Math.min(HARD_LIMIT, Integer.parseInt(args[0]));
        }

        CompletableFuture<SeasonSearch> seasonSearchCompletableFuture = new Connector().seasonLater();
        SeasonSearch seasonSearch = seasonSearchCompletableFuture.get();
        List<SeasonSearchAnime> animes = seasonSearch.getAnimes().subList(0, numAnimes);


        for(SeasonSearchAnime anime: animes) {
            event.reply(new EmbedBuilder()
                    .setColor(event.isFromType(ChannelType.TEXT) ? event.getSelfMember().getColor() : Color.GREEN)
                    .setDescription(anime.getSynopsis())
                    .setImage(anime.getImageURL())
                    .build());
        }

    }
}
