package com.jojonicho.animebot.anime.commands;

import com.github.doomsdayrs.jikan4java.core.Connector;
import com.github.doomsdayrs.jikan4java.data.enums.Season;
import com.github.doomsdayrs.jikan4java.data.model.main.season.SeasonSearch;
import com.github.doomsdayrs.jikan4java.data.model.main.season.SeasonSearchAnime;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import lombok.SneakyThrows;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.ChannelType;

import java.awt.*;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class SeasonSearchCommand extends Command {

    public SeasonSearchCommand() {
        this.name = "season-search";
        this.aliases = new String[]{"ss"};
        this.help = "search anime from year and season";
        this.arguments = "<year> <summer/winter/spring/fall>";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
    }

    @SneakyThrows
    @Override
    protected void execute(CommandEvent event) throws NumberFormatException {

        String[] args  = event.getArgs().split(" ");
        int year = Integer.parseInt(args[0]);
        Season season = null;

        if(args[1].equalsIgnoreCase("summer")) {
            season = Season.SUMMER;
        } else if (args[1].equalsIgnoreCase("winter")) {
            season = Season.WINTER;
        } else if (args[1].equalsIgnoreCase("spring")) {
            season = Season.SPRING;
        } else if (args[1].equalsIgnoreCase("fall")) {
            season = Season.FALL;
        }
        if (null == season) {
            event.reactError();
            return;
        }

        CompletableFuture<SeasonSearch> search = new Connector().seasonSearch(year, season);
        SeasonSearch seasonSearch = search.get();
        List<SeasonSearchAnime> animes = seasonSearch.getAnimes().subList(0, 5);

        for(SeasonSearchAnime anime: animes) {
            String title = String.format("%d - %s", anime.getMalID(), anime.getTitle());
            event.reply(new EmbedBuilder()
                    .setColor(event.isFromType(ChannelType.TEXT) ? event.getSelfMember().getColor() : Color.GREEN)
                    .setTitle(title)
                    .setDescription(anime.getSynopsis())
                    .setImage(anime.getImageURL())
                    .build());
        }
    }
}
