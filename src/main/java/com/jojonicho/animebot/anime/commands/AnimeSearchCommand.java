/*
 * Copyright 2017 John Grosh (jagrosh).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jojonicho.animebot.anime.commands;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.doomsdayrs.jikan4java.core.Retriever;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.anime.model.JikanAnime;
import com.jojonicho.animebot.anime.model.JikanAnimeList;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.async.Callback;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.SneakyThrows;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.ChannelType;
import org.json.JSONArray;
import org.json.JSONObject;

import java.awt.*;
import java.util.Map;
import java.util.concurrent.CompletableFuture;


public class AnimeSearchCommand extends Command {

    private ObjectMapper objectMapper;

    public AnimeSearchCommand() {
        this.name = "anime-search";
        // tambah singkatan
        this.aliases = new String[]{"as"};
        this.help = "search for anime";
        this.arguments = "<query>";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
        this.objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
    }

    @SneakyThrows
    @Override
    protected void execute(CommandEvent event) {
        String[] args  = event.getArgs().split("\\s+");
        String query = String.join("%20", args);
        String URL = String.format("https://api.jikan.moe/v3/search/anime?q=%s", query);

        try {
            CompletableFuture<JikanAnimeList> animeSearch =
                    new Retriever().retrieve(URL, JikanAnimeList.class);

            JikanAnimeList lst = animeSearch.get();
            JikanAnime anime = lst.getResults().get(0);

            String title = String.format("%d - %s", anime.getMalId(), anime.getTitle());
            event.reply(new EmbedBuilder()
                    .setColor(event.isFromType(ChannelType.TEXT) ? event.getSelfMember().getColor() : Color.GREEN)
                    .setTitle(title)
                    .setImage(anime.getImageUrl())
                    .setDescription(anime.getSynopsis())
                    .build());
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }

        unirestCallback(event, URL);
    }

    private void unirestCallback(CommandEvent event, String url) {
        // unirest version, less clean, gets called if wrapper throws exception
        Unirest.get(url).asJsonAsync(new Callback<JsonNode>() {
            // The API call was successful
            @Override
            public void completed(HttpResponse<JsonNode> hr) {
                JsonNode jsonNode = hr.getBody();
                JSONObject obj = jsonNode.getObject();
                JSONArray results = obj.getJSONArray("results");

                Map<String, Object> animeMp = objectMapper.convertValue(results.get(0), Map.class);
                Object animeObj = animeMp.get("map");

                try {
                    JikanAnime anime = objectMapper.convertValue(animeObj, JikanAnime.class);

                    event.reply(new EmbedBuilder()
                            .setColor(event.isFromType(ChannelType.TEXT) ? event.getSelfMember().getColor() : Color.GREEN)
                            .setImage(anime.getImageUrl())
                            .setDescription("pong")
                            .build());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // The API call failed
            @Override
            public void failed(UnirestException ue)
            {
                event.reactError();
            }

            // The API call was cancelled (this should never happen)
            @Override
            public void cancelled()
            {
                event.reactError();
            }
        });
    }
}
