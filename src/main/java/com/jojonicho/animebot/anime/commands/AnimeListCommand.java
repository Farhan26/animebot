package com.jojonicho.animebot.anime.commands;

import com.github.doomsdayrs.jikan4java.core.Connector;
import com.github.doomsdayrs.jikan4java.data.model.main.anime.Anime;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import lombok.SneakyThrows;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.ChannelType;

import java.awt.*;

public class AnimeListCommand extends Command {

    private AnimeEntryService animeEntryService;
    private DiscordUserService discordUserService;

    public AnimeListCommand(AnimeEntryService animeEntryService, DiscordUserService discordUserService) {
        this.name = "anime-list";
        this.aliases = new String[]{"al"};
        this.help = "get list of added anime";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
        this.animeEntryService = animeEntryService;
        this.discordUserService = discordUserService;
    }

    @SneakyThrows
    @Override
    protected void execute(CommandEvent event) {

        String discordUserId = event.getAuthor().getId();

        Iterable<AnimeEntry> animeEntryList = animeEntryService.getListAnimeEntry(discordUserId);

        if (!animeEntryList.iterator().hasNext()) {
            event.reactError();
            return;
        }

        String title = String.format("%s's anime list", event.getAuthor().getName());
        String animeImageUrl = null;
        String avatarImageUrl = event.getAuthor().getAvatarUrl();
        StringBuilder description = new StringBuilder();

        for(AnimeEntry animeEntry: animeEntryList) {
            int malId = animeEntry.getMalId();
            Anime anime = new Connector().retrieveAnime(malId).get();

            description.append(String.format("%d - %s - %s - %d\n",
                    malId, anime.getTitle(), animeEntry.getStatus(), animeEntry.getRating()));

            if (null == animeImageUrl) {
                animeImageUrl = anime.getImageURL();
            }
        }

        event.reply(new EmbedBuilder()
                .setColor(event.isFromType(ChannelType.TEXT) ? event.getSelfMember().getColor() : Color.GREEN)
                .setTitle(title)
                .setDescription(description.toString())
                .setImage(animeImageUrl)
                .setThumbnail(avatarImageUrl)
                .build()
        );
    }

}
