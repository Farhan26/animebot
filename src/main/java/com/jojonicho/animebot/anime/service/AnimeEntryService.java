package com.jojonicho.animebot.anime.service;


import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.discorduser.model.DiscordUser;

public interface AnimeEntryService {
    AnimeEntry createAnimeEntry(int malId, DiscordUser discordUser);

    AnimeEntry getAnimeEntry(int malId, DiscordUser discordUser);

    Iterable<AnimeEntry> getListAnimeEntry(String userId);

    AnimeEntry updateAnimeEntry(AnimeEntry animeEntry);

    void deleteAnimeEntry(AnimeEntry animeEntry);


//    AnimeEntry getAnimeEntry(String npm);

//    AnimeEntry getAnimeEntryBy
//    Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa);

//    void deleteMahasiswaByNPM(String npm);
}
