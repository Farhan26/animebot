package com.jojonicho.animebot.anime.service;

import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.anime.repository.AnimeEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnimeEntryServiceImpl implements AnimeEntryService {
    @Autowired
    private AnimeEntryRepository animeEntryRepository;

    @Override
    public AnimeEntry createAnimeEntry(int malId, DiscordUser discordUser) {

        AnimeEntry animeEntry = getAnimeEntry(malId, discordUser);
        if (null != animeEntry) {
            return animeEntry;
        }

        animeEntry = new AnimeEntry();
        animeEntry.setMalId(malId);
        animeEntry.setDiscordUser(discordUser);

        return animeEntryRepository.save(animeEntry);
    }

    @Override
    public Iterable<AnimeEntry> getListAnimeEntry(String userId) {
        return animeEntryRepository.findAllByDiscordUser_Id(userId);
    }

    @Override
    public AnimeEntry updateAnimeEntry(AnimeEntry animeEntry) {
        return animeEntryRepository.save(animeEntry);
    }

    @Override
    public void deleteAnimeEntry(AnimeEntry animeEntry) {
        animeEntryRepository.delete(animeEntry);
    }

    @Override
    public AnimeEntry getAnimeEntry(int malId, DiscordUser discordUser) {
        return animeEntryRepository.findByMalIdAndDiscordUser(malId, discordUser);
    }


//    @Autowired
//    private MahasiswaRepository mahasiswaRepository;
//
//    @Override
//    public Mahasiswa createMahasiswa(Mahasiswa mahasiswa) {
//        mahasiswaRepository.save(mahasiswa);
//        return mahasiswa;
//    }
//
//    @Override
//    public Iterable<Mahasiswa> getListMahasiswa() {
//        return mahasiswaRepository.findAll();
//    }
//
//    @Override
//    public Mahasiswa getMahasiswaByNPM(String npm) {
//        return mahasiswaRepository.findByNpm(npm);
//    }
//
//    @Override
//    public Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa) {
//        mahasiswa.setNpm(npm);
//        mahasiswaRepository.save(mahasiswa);
//        return mahasiswa;
//    }
//
//    @Override
//    public void deleteMahasiswaByNPM(String npm) {
//        Mahasiswa mahasiswa = getMahasiswaByNPM(npm);
//        if (null == mahasiswa) {
//            return;
//        }
//        if (mahasiswa.getMataKuliah() == null) {
//            mahasiswaRepository.delete(mahasiswa);
//        }
//    }
}
