package com.jojonicho.animebot.anime.repository;

import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnimeEntryRepository extends JpaRepository<AnimeEntry, String> {
    AnimeEntry findByMalIdAndDiscordUser(int malId, DiscordUser discordUser);
    Iterable<AnimeEntry> findAllByDiscordUser_Id(String id);
}
