package com.jojonicho.animebot.discorduser.service;


import com.jojonicho.animebot.discorduser.model.DiscordUser;

public interface DiscordUserService {
//    AnimeEntry createAnimeEntry(AnimeEntry animeEntry);
    DiscordUser createDiscordUser(String id);
    DiscordUser getDiscordUser(String id);

//    Iterable<AnimeEntry> getListAnimeEntry(String userId);

//    AnimeEntry getAnimeEntry(String npm);

//    AnimeEntry getAnimeEntryBy
//    Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa);

//    void deleteMahasiswaByNPM(String npm);
}
