package com.jojonicho.animebot.discorduser.service;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.anime.repository.AnimeEntryRepository;
import com.jojonicho.animebot.discorduser.repository.DiscordUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DiscordUserServiceImpl implements DiscordUserService {
    @Autowired
    private AnimeEntryRepository animeEntryRepository;

    @Autowired
    private DiscordUserRepository discordUserRepository;

    @Override
    public DiscordUser createDiscordUser(String id) {
        DiscordUser discordUser = new DiscordUser();
        discordUser.setId(id);
        return discordUserRepository.save(discordUser);
    }

    @Override
    public DiscordUser getDiscordUser(String id) {
        DiscordUser discordUser = discordUserRepository.getDiscordUserById(id);
//        try {
//            discordUser = discordUserRepository.getDiscordUserById(id);
//        } catch (NullPointerException e) {
//            discordUser = new DiscordUser();
//            discordUser.setId(id);
//            return discordUserRepository.save(discordUser);
//        }
        if (null == discordUser) {
           discordUser = new DiscordUser();
           discordUser.setId(id);
           discordUser = discordUserRepository.save(discordUser);
        }
        return discordUser;
    }

//    @Override
//    public AnimeEntry createAnimeEntry(AnimeEntry animeEntry) {
//        return animeEntryRepository.save(animeEntry);
//    }
//
//    @Override
//    public Iterable<AnimeEntry> getListAnimeEntry(String userId) {
//        return animeEntryRepository.findAllByDiscordUser_Id(userId);
//    }
////    @Autowired
//    private MahasiswaRepository mahasiswaRepository;
//
//    @Override
//    public Mahasiswa createMahasiswa(Mahasiswa mahasiswa) {
//        mahasiswaRepository.save(mahasiswa);
//        return mahasiswa;
//    }
//
//    @Override
//    public Iterable<Mahasiswa> getListMahasiswa() {
//        return mahasiswaRepository.findAll();
//    }
//
//    @Override
//    public Mahasiswa getMahasiswaByNPM(String npm) {
//        return mahasiswaRepository.findByNpm(npm);
//    }
//
//    @Override
//    public Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa) {
//        mahasiswa.setNpm(npm);
//        mahasiswaRepository.save(mahasiswa);
//        return mahasiswa;
//    }
//
//    @Override
//    public void deleteMahasiswaByNPM(String npm) {
//        Mahasiswa mahasiswa = getMahasiswaByNPM(npm);
//        if (null == mahasiswa) {
//            return;
//        }
//        if (mahasiswa.getMataKuliah() == null) {
//            mahasiswaRepository.delete(mahasiswa);
//        }
//    }
}
