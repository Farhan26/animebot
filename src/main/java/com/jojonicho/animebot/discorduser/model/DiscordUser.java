package com.jojonicho.animebot.discorduser.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "discord_user")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DiscordUser {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

//    @JsonIgnore
//    @OneToMany(mappedBy = "discordUser", fetch = FetchType.EAGER)
//    private List<AnimeEntry> animeEntries;
}
