package com.jojonicho.animebot.discorduser.repository;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiscordUserRepository extends JpaRepository<DiscordUser, String> {
//    AnimeEntry findById(int id);
//    DiscordUser findById(String id);
    DiscordUser getDiscordUserById(String id);
//    Iterable<Log> findAllByAsisten_Npm(String npm);
//    Iterable<AnimeEntry> findAllByDiscordUser_Id(String id);
}
