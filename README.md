# ExampleBot
A bot showcasing the Command Client in the JDA-Utilities library, extended with Jikan4Java, Postgres, and Gradle.

# Dependencies
* [JDA](https://github.com/DV8FromTheWorld/JDA)
* [JDA-Utilities](https://github.com/JDA-Applications/JDA-Utilities)
* Jikan4Java
* Postgres

# Prerequisites
1. replace `src/main/resources/application.yml.example` with `src/main/resources/application.yml` and don't forget to add your own bot's token
2. make sure postgres is running, customize it in `src/main/resources/application.properties`

# Resources
1. [Setup Bot, get token, and invite to server](https://www.youtube.com/watch?v=jGrD8AZfTig&ab_channel=techtoolbox)
2. [Read about JDA](https://www.youtube.com/watch?v=jGrD8AZfTig&ab_channel=techtoolbox)
3. [Read about Jikan4Java](https://www.youtube.com/watch?v=jGrD8AZfTig&ab_channel=techtoolbox)
4. [Jikan API Docs](https://jikan.docs.apiary.io/)